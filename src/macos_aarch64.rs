use core::arch::asm;

pub fn hello_world(hello: &[u8], len: usize) -> ! {
    unsafe {
        // sys_write(hello, len)
        asm!(
            "mov x0, #1",
            "mov x1, {hello}",
            "mov x2, {len}",
            "mov x16, #4",
            "svc 0",
            hello = in(reg) hello.as_ptr(),
            len = in(reg) len,
            out("x0") _,
            out("x1") _,
            out("x2") _,
            out("x16") _,
        );

        // exit(0)
        asm!(
            "mov x0, #0",
            "mov x16, #1",
            "svc 0",
            out("x0") _,
            out("x16") _,
        );
    }

    loop {}
}
