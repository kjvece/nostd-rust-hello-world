#![no_std]
#![no_main]

#[panic_handler]
fn panic(_info: &core::panic::PanicInfo) -> ! {
    loop {}
}

#[cfg(all(target_os = "macos", target_arch = "aarch64"))]
mod macos_aarch64;
#[cfg(all(target_os = "macos", target_arch = "aarch64"))]
use macos_aarch64::hello_world;

#[cfg(all(target_os = "macos", target_arch = "x86_64"))]
mod macos_x86_64;
#[cfg(all(target_os = "macos", target_arch = "x86_64"))]
use macos_x86_64::hello_world;

#[cfg(all(target_os = "linux", target_arch = "aarch64"))]
mod linux_aarch64;
#[cfg(all(target_os = "linux", target_arch = "aarch64"))]
use linux_aarch64::hello_world;

#[cfg(all(target_os = "linux", target_arch = "x86_64"))]
mod linux_x86_64;
#[cfg(all(target_os = "linux", target_arch = "x86_64"))]
use linux_x86_64::hello_world;

#[no_mangle]
pub extern "C" fn _start(_argc: i32, _argv: *const *const u8) -> ! {
    let hello: &[u8] = b"hello, world!\n\0";
    let len = hello.len() - 1;
    hello_world(hello, len);
}
