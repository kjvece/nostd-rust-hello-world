use core::arch::asm;

pub fn hello_world(hello: &[u8], len: usize) -> ! {
    unsafe {
        // sys_write(hello, len)
        asm!(
            "mov rdi, 1",
            "mov rsi, {hello}",
            "mov rdx, {len}",
            "mov rax, 0x2000004",
            "syscall",
            hello = in(reg) hello.as_ptr(),
            len = in(reg) len,
            out("rdi") _,
            out("rsi") _,
            out("rdx") _,
            out("rax") _,
        );

        asm!(
            "xor rdi, rdi",
            "mov rax, 0x2000001",
            "syscall",
            out("rdi") _,
            out("rax") _,
        );
    }
    loop {}
}
