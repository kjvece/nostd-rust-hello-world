fn main() {
    let target = std::env::var("TARGET").unwrap();
    if target.contains("apple") {
        println!("cargo:rustc-link-arg=-e__start");
        println!("cargo:rustc-link-arg=-lSystem");
        println!("cargo:rustc-link-arg=-nostdlib");
        println!("cargo:rustc-link-arg=-nostartfiles");
    } else if target.contains("linux") {
        println!("cargo:rustc-link-arg=-nostdlib");
        println!("cargo:rustc-link-arg=-nostartfiles");
    } else if target.contains("windows") {
        println!("cargo:rustc-link-arg=/ENTRY:_start");
        println!("cargo:rustc-link-arg=/SUBSYSTEM:console");
    }
}
