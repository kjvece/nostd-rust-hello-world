# nostd rust

A minimal hello world example of a no std binary in rust

## Implemented and tested

- [x] macos aarch64
- [x] macos x86_64
- [x] linux aarch64
- [x] linux x86_64
- [ ] windows aarch64
- [ ] windows x86_64
